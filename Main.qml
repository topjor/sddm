import QtQuick 2.0
import SddmComponents 2.0
import QtGraphicalEffects 1.0

// page
Item {
  id: page
  width: 1920
  height: 1080

  // background image
  Image {
    id: background
    anchors.fill: parent
    source: config.background
  }

  // login box background
  Item {
    id: mainView

    width: 750
    height: 500

    anchors.horizontalCenter: page.horizontalCenter
    anchors.verticalCenter: page.verticalCenter

    // image for blur (disabled)
    Image {
      id: mainBackground
      anchors.fill: mainView

      source: config.background

      smooth: true
      visible: false
    }

    // blur (disabled)
    GaussianBlur {
      id: mainViewBackgroundBlur
      anchors.fill: mainView

      deviation: 4
      radius: 8
      samples: 16

      visible: false

      source: mainBackground
    }

    // nice white half transparent box
    Rectangle {
      id: mainViewBackground

      anchors.fill: mainView
      color: "white"
      opacity: 0.3
      radius: 5
    }
    // border for the box
    Rectangle {
      id: mainViewBorder

      anchors.fill: mainView
      color: "transparent"
      opacity: 0.5
      radius: 5
      border.width: 1
      border.color: "white"
    }
  }

  // login frame (after user select)
  Login {
    id: loginFrame
    visible: false
    opacity: 0
  }

  // user select list
  UserList {
    id: userList
    visible: true
    opacity: 1
  }


  states: State {
    name: "login"
    PropertyChanges {
      target: userList
      visible: false
      opacity: 0
    }

    PropertyChanges {
      target: loginFrame
      visible: true
      opacity: 1
    }
  }

  transitions: [
  Transition {
    from: ""
    to: "login"
    reversible: false

    SequentialAnimation {
      PropertyAnimation {
        target: userList
        properties: "opacity"
        duration: 200
      }
      PropertyAnimation {
        target: userList
        properties: "visible"
        duration: 0
      }
      PropertyAnimation {
        target: loginFrame
        properties: "visible"
        duration: 0
      }
      PropertyAnimation {
        target: loginFrame
        properties: "opacity"
        duration: 200
      }
    }
  },
  Transition {
    from: "login"
    to: ""
    reversible: false

    SequentialAnimation {
      PropertyAnimation {
        target: loginFrame
        properties: "opacity"
        duration: 200
      }
      PropertyAnimation {
        target: loginFrame
        properties: "visible"
        duration: 0
      }
      PropertyAnimation {
        target: userList
        properties: "visible"
        duration: 0
      }
      PropertyAnimation {
        target: userList
        properties: "opacity"
        duration: 200
      }
    }
  }]
}
