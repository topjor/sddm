import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {

  width: 250
  height: 250

  property string name: model.name
  property string realName: (model.realName === "") ? model.name : model.realName
  property string icon: model.icon

  //User's Name
  Text {
    id: userNameShadow

    color: "#ccc"
    font {
      family: "monospace"
      pointSize: 22
    }
    text: realName
    anchors {
      horizontalCenter: parent.horizontalCenter
      horizontalCenterOffset: 1
      top: userPicture.bottom
      topMargin: 15
    }
  }
  Text {
    id: userName

    color: "black"
    font {
      family: "monospace"
      pointSize: 22
    }
    text: realName
    anchors {
      horizontalCenter: parent.horizontalCenter
      top: userPicture.bottom
      topMargin: 15
    }
  }

  //User's Profile Pic
  Item {
    id: userPicture

    width: 128
    height: 128

    anchors {
      top: parent.top
      topMargin: 50
      horizontalCenter: parent.horizontalCenter
    }

    Image {
    id: userPictureIcon

    anchors.fill: userPicture
    width: 128
    height: 128

    source: icon

    }

    Rectangle {
      id: userPictureBorder

      anchors.fill: userPicture
      color: "transparent"
      border.width: 1
      border.color: "white"
    }
  }


  MouseArea {
    anchors.fill: parent
    onClicked: {
      userList.currentIndex = index;
      page.state = "login";
      loginFrame.name = name
      loginFrame.realName = realName
      loginFrame.icon = icon
      userList.focus = false

	  if (config.autoFocusPassword == "true")
		  focusDelay.start();
	  else
      loginFrame.focus = true;
    }
  }

}
